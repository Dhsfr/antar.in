<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class homeController extends Controller{

    public function index(){
        return view('user/home/index');

    }

    public function create(){
        

    }

    public function store(Request $request){
        

    }

    public function show($id){
        

    }

    public function edit($id){
        

    }

    public function update(Request $request, $id){
        

    }

    public function destroy($id){
        

    }
}
